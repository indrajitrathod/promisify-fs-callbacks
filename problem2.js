const fs = require('fs');
const path = require('path');
const FILETOREAD = path.join(__dirname, './lipsum.txt');

const readFile = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (error, fileData) => {
            if (error) {
                console.error(`ERROR: Reading File ${filePath}`, error);
                reject(filePath);
            } else {
                console.log(`SUCCESS: Reading File ${filePath}`);
                resolve(fileData);
            }
        });
    });
}

const writeFile = (filePath, data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, data, (error) => {
            if (error) {
                reject(filePath);
            } else {
                resolve(filePath);
            }
        });
    })
};

const writeFileName = (fileNameHolder, fileName) => {
    const fileNametoBeStored = fileName.concat('\n');
    return new Promise((resolve, reject) => {
        fs.writeFile(fileNameHolder, fileNametoBeStored, { flag: 'a+' }, (error) => {
            if (error) {
                reject(fileName);
            } else {
                resolve(fileName);
            }
        });
    });
}

const deleteFile = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, (error) => {
            if (error) {
                console.error(`ERROR: Deleting File ${filePath}`);
                reject(filePath);
            } else {
                console.log(`SUCCESS: Deleting File ${filePath}`);
                resolve(filePath);
            }
        });
    });
}

const createReadWriteDeleteFiles = () => {
    const fileNameHolder = path.join(__dirname, './filenames.txt');
    const upperCasedFilePath = path.join(__dirname, 'uppercased_lipsum.txt');
    const lowerCasedAndSentencedFilePath = path.join(__dirname, 'lowercased_and_sentenced.txt');
    readFile(FILETOREAD)
        .then((fileData) => {
            const upperCasedData = fileData.toUpperCase();
            return writeFile(upperCasedFilePath, upperCasedData);
        })
        .then((upperCasedFilePath) => {
            console.log(`SUCCESS: Writing uppercased data to ${upperCasedFilePath}`);
            return writeFileName(fileNameHolder, upperCasedFilePath);
        })
        .then((upperCasedFilePath) => {
            console.log(`SUCCESS: Writing filename ${upperCasedFilePath} in file ${fileNameHolder}`);
            return readFile(upperCasedFilePath);
        })
        .then((upperCasedData) => {
            const lowerCasedAndSentencedData = upperCasedData.toLowerCase().split('.')
                .map((sentence) => {
                    return sentence.trim();
                })
                .filter((sentence) => {
                    return sentence !== '';
                })
                .join('\n');
            return writeFile(lowerCasedAndSentencedFilePath, lowerCasedAndSentencedData);
        })
        .then((lowerCasedAndSentencedFilePath) => {
            console.log(`SUCCESS: writing lowercased and sentenced data to ${lowerCasedAndSentencedFilePath}`);
            return writeFileName(fileNameHolder, lowerCasedAndSentencedFilePath);
        })
        .then((lowerCasedAndSentencedFilePath) => {
            console.log(`SUCCESS: Writing filename ${lowerCasedAndSentencedFilePath} in file ${fileNameHolder}`);
            return readFile(upperCasedFilePath);
        })
        .then((upperCasedData) => {
            const sortedUpperCasedData = upperCasedData.replaceAll(' ', '').replaceAll('\n', '').split('').sort().join('');
            const sortedUpperCaseFileName = path.join(__dirname, 'uppercase_sorted.txt');
            return writeFile(sortedUpperCaseFileName, sortedUpperCasedData);
        })
        .then((sortedUpperCaseFilePath) => {
            console.log(`SUCCESS: writing sorted uppercased data in file ${sortedUpperCaseFilePath}`);
            return writeFileName(fileNameHolder, sortedUpperCaseFilePath);
        })
        .then((sortedUpperCaseFilePath) => {
            console.log(`SUCCESS: Writing filename ${sortedUpperCaseFilePath} in file ${fileNameHolder}`);
            return readFile(lowerCasedAndSentencedFilePath);
        })
        .then((lowerCasedAndSentencedFileData) => {
            const sortedLowerCasedAndSentencedData = lowerCasedAndSentencedFileData.replaceAll(' ', '').replaceAll('\n', '').split('').sort().join('');
            const sortedlowerCasedAndSentencedFileName = path.join(__dirname, 'lowedcased_sentenced_sorted.txt');
            return writeFile(sortedlowerCasedAndSentencedFileName, sortedLowerCasedAndSentencedData);
        })
        .then((sortedlowerCasedAndSentencedFilePath) => {
            console.log(`SUCCESS: writing sorted lowercased and sentenced data in file ${sortedlowerCasedAndSentencedFilePath}`);
            return writeFileName(fileNameHolder, sortedlowerCasedAndSentencedFilePath);
        })
        .then((sortedlowerCasedAndSentencedFilePath) => {
            console.log(`SUCCESS: Writing filename ${sortedlowerCasedAndSentencedFilePath} in file ${fileNameHolder}`);
            return readFile(fileNameHolder);
        })
        .then((filePaths) => {
            console.log(`Deleting files listed in ${fileNameHolder}`);
            const filesToBeDeleted = filePaths.split('\n')
                .filter((filePath) => {
                    return filePath !== '';
                })
                .map((fileToDelete) => {
                    return deleteFile(fileToDelete);
                });
            return Promise.all(filesToBeDeleted);
        })
        .then((filesDeleted) => {
            console.log(`ALL FILES DELETED SUCCESSFULLY`);
            console.log(filesDeleted);
            console.log(`DONE!!!`);
        })
        .catch((error) => {
            console.error(error);

        });
}

module.exports = createReadWriteDeleteFiles;

