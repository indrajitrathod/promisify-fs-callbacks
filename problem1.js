const fs = require('fs');
const path = require('path');

const createDirectory = (dirName) => {
    const dirPath = path.join(__dirname, dirName);
    return new Promise((resolve, reject) => {
        fs.mkdir(dirPath, (error) => {
            if (error && error.code !== 'EEXIST') {
                console.error(`COULDN'T CREATE DIRECTORY`);
                console.error(`ERROR: Creating Directory- ${dirName}`, error);
                reject(error);
            } else {
                if (error && error.code === 'EEXIST') {
                    console.log(`SKIPPING DIRECTORY CREATION, AS IT ALREADY EXISTS`);
                } else {
                    console.log(`SUCCESS: Creating Directory- ${dirName} with directory Path- ${dirPath}`);
                }
                resolve(dirPath);
            }
        });
    });
}

const createFile = (dirPath, fileName, fileData) => {
    const filePath = path.join(dirPath, fileName);
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, fileData, (error) => {
            if (error) {
                console.error(`ERROR: Creating File ${fileName}`, error);
                reject(error);
            } else {
                console.log(`SUCCESS: Creating File ${fileName} with file path- ${filePath}`);
                resolve(filePath);
            }
        });
    });
}

const deleteFile = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, (error) => {
            if (error) {
                console.error(`ERROR: Deleting File ${filePath}`);
                reject(error);
            } else {
                console.log(`SUCCESS: Deleting File ${filePath}`);
                resolve(filePath);
            }
        });
    });
}

const createRandomJSONfiles = (dirPath) => {
    const UPPERLIMITFILES = 10;
    const numberOfFiles = Math.round(Math.random() * UPPERLIMITFILES);
    const iterateArray = new Array(numberOfFiles).fill(1);
    const fileNames = iterateArray.map((element, index) => {
        const RANDOMNUMBERLIMIT = 100;
        const randomNumber = Math.round(Math.random() * RANDOMNUMBERLIMIT);
        const fileName = `${index}_${randomNumber}.json`;
        return fileName;
    });

    const filesTocreate = fileNames.map((fileName) => {
        const dateAndTime = new Date().toUTCString();
        let fileContent = {
            fileName: fileName,
            created_at: dateAndTime
        };

        try {
            fileContent = JSON.stringify(fileContent, null, 4);
        } catch (error) {
            console.error(`ERROR: Coverting JSON.stringify`);
        }

        return createFile(dirPath, fileName, fileContent);
    });

    return new Promise((resolve, reject) => {
        Promise.all(filesTocreate)
            .then((createdFiles) => {
                resolve(createdFiles);
            })
            .catch((errors) => {
                reject(errors);
            });
    });
}

const deleteFiles = (filePaths) => {
    return new Promise((resolve, reject) => {
        const filesToDelete = filePaths.map((filePath) => {
            return deleteFile(filePath);
        });
        Promise.all(filesToDelete)
            .then((successDeleted) => {
                resolve(successDeleted);
            })
            .catch((errors) => {
                reject(errors);
            });
    });
}

const createDirectoryAndRandomJSONFilesAndDelete = () => {
    const directoryName = 'RandomJSONFiles';
    createDirectory(directoryName)
        .then((directoryPath) => {
            console.log(`CREATING RANDOM JSON FILES`);
            return createRandomJSONfiles(directoryPath);
        })
        .then((createdFiles) => {
            console.log(`Created Files`, createdFiles);
            console.log(`Deleting These files`);
            return deleteFiles(createdFiles);
        })
        .then((deletedFiles) => {
            console.log(`ALL Files DELETED Successfully`);
            console.log(deletedFiles);
        })
        .catch((error) => {
            console.error(error);
        });
}

module.exports = createDirectoryAndRandomJSONFilesAndDelete;